<!DOCTYPE HTML>
<html lang="en">
  <head>
    <meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Travis CI User Documentation </title>
<link rel="stylesheet" href="/assets/stylesheets/main.css">
<link rel="alternate" type="application/rss+xml" title="Travis CI Build Environment Updates" href="https://docs.travis-ci.com/feed.build-env-updates.xml">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js" defer></script>

<script src="/assets/javascripts/main.js" type="text/javascript" charset="utf-8" defer></script>
<script src="/assets/javascripts/prism.js" type="text/javascript" charset="utf-8" defer></script>
<meta name="twitter:card" content="summary" />
<meta name="twitter:site" content="@travisci" />
<meta name="twitter:creator" content="@travisci" />
<meta property="og:title" content="Travis CI Documentation" />
<meta property="og:type" content="website" />
<meta property="og:url" content=/ />
<meta property="og:description" content="Travis CI User Documentation" />
<meta property="og:image" content="/images/TravisCI-Full-Color.png" }} />
<meta property="og:image:type" content="image/png" />
<meta property="og:image:width" content="642" />
<meta property="og:image:height" content="201" />
<meta property="og:image:alt" content="Travis CI logo" />




<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-24868285-6']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>

  </head>
  <body>
    <div class="wrapper">

      <header class="top">
  <div class="row topbar">
    <h1 id="logo" class="logo">
  <a href="https://travis-ci.com/" title="Travis CI">Travis</a>
</h1>

    <nav>
      <ul id="navigation" class="navigation">
        <li><a href="https://blog.travis-ci.com">Blog</a></li>
        <li><a href="/">Docs</a></li>
        <li class="toggle"><button type="button" id="toggle-menu" class="button--teal">Menu</button></li>
      </ul>
    </nav>
  </div>
</header>


      <div id="content" class="row">

        <div id="sidebar" class="sidebar">
  <div>
    <!-- <h2>Search</h2> -->
    <div class="searchbox">
      <form>
        <input type="text" id="st-search-input" class="searchbox-input" placeholder="Search the docs" />
      </form>
      <script type="text/javascript">
      var Swiftype = window.Swiftype || {};
        (function() {
          Swiftype.key = 'tXvDDzd4fNTBnLvxfEyx';

          Swiftype.searchSearchFields = {
            "page": ["title, body, tags"]
          };
          Swiftype.autocompleteSearchFields = {
            "page": ["title, body, tags"]
          };
          /** DO NOT EDIT BELOW THIS LINE **/
          var script = document.createElement('script'); script.type = 'text/javascript'; script.async = true;
          script.src = "//swiftype.com/embed.js";
          var entry = document.getElementsByTagName('script')[0];
          document.getElementsByTagName('script')[0].parentNode.insertBefore(script, entry);
        }());
      </script>
    </div>
  </div>

  <section class="sidebar-navigation">
    <nav>
      <h3>Getting Started</h3>
      <ul>
        <li><a href="/user/for-beginners/">Core Concepts for Beginners</a></li>
        <li><a href="/user/tutorial/">Travis CI Tutorial</a></li>
        <li><a href="/user/customizing-the-build/">Customizing the Build</a></li>
        <li><a href="/user/speeding-up-the-build/">Speeding up the Build</a></li>
        <li><a href="/user/docker/">Using Docker in your Build</a></li>
        <li><a href="/user/gui-and-headless-browsers/">Headless Testing with Browsers</a></li>
        <li><a href="/user/pull-requests/">Building Pull Requests</a></li>
        <li><a href="/user/cron-jobs/">Cron Jobs</a></li>
        <li><a href="/user/common-build-problems/">Common Build Problems</a></li>
        <li><a href="https://github.com/travis-ci/travis#readme">Command Line Client</a></li>
        <li><a href="/user/build-config-imports/">Build Config Imports</a></li>
        <li><a href="/user/build-config-validation/">Build Config Validation</a></li>
      </ul>

      <h3>Jobs, Builds, Matrices and Stages</h3>
      <ul>
        <li><a href="/user/job-lifecycle/">Job Lifecycle</a></li>
        <li><a href="/user/build-matrix/">Build Matrix</a></li>
        <li><a href="/user/build-stages/">Build Stages</a></li>
        <li><a href="/user/conditional-builds-stages-jobs/">Conditional Builds, Stages, and Jobs</a></li>
      </ul>

      <h3>Installing Dependencies</h3>
      <ul>
        <li><a href="/user/installing-dependencies/">Installing Dependencies</a></li>
        <li><a href="/user/private-dependencies/">Private Dependencies GitHub</a></li>
        <li><a href="/user/private-dependencies-bb/">Private Dependencies Bitbucket</a></li>
        <li><a href="/user/database-setup/">Setting up Services and Databases</a></li>
        <li><a href="/user/caching/">Caching Dependencies</a></li>
        <li><a href="/user/ssh-known-hosts/">Adding to SSH Known Hosts</a></li>
      </ul>

      <h3>Programming Languages</h3>
      <ul>
      
        <li><a href="/user/languages/android/">Android</a></li>
      
        <li><a href="/user/languages/c/">C</a></li>
      
        <li><a href="/user/languages/csharp/">C#</a></li>
      
        <li><a href="/user/languages/cpp/">C++</a></li>
      
        <li><a href="/user/languages/clojure/">Clojure</a></li>
      
        <li><a href="/user/languages/crystal/">Crystal</a></li>
      
        <li><a href="/user/languages/d/">D</a></li>
      
        <li><a href="/user/languages/dart/">Dart</a></li>
      
        <li><a href="/user/languages/elixir/">Elixir</a></li>
      
        <li><a href="/user/languages/elm/">Elm</a></li>
      
        <li><a href="/user/languages/erlang/">Erlang</a></li>
      
        <li><a href="/user/languages/csharp/">F#</a></li>
      
        <li><a href="/user/languages/minimal-and-generic/">Generic</a></li>
      
        <li><a href="/user/languages/go/">Go</a></li>
      
        <li><a href="/user/languages/groovy/">Groovy</a></li>
      
        <li><a href="/user/languages/haskell/">Haskell</a></li>
      
        <li><a href="/user/languages/haxe/">Haxe</a></li>
      
        <li><a href="/user/languages/java/">Java</a></li>
      
        <li><a href="/user/languages/javascript-with-nodejs/">JavaScript <small>(with Node.js)</small></a></li>
      
        <li><a href="/user/languages/julia/">Julia</a></li>
      
        <li><a href="/user/languages/matlab/">MATLAB</a></li>
      
        <li><a href="/user/languages/minimal-and-generic/">Minimal</a></li>
      
        <li><a href="/user/languages/nix/">Nix</a></li>
      
        <li><a href="/user/languages/objective-c/">Objective-C</a></li>
      
        <li><a href="/user/languages/perl/">Perl</a></li>
      
        <li><a href="/user/languages/perl6/">Perl6</a></li>
      
        <li><a href="/user/languages/php/">PHP</a></li>
      
        <li><a href="/user/languages/python/">Python</a></li>
      
        <li><a href="/user/languages/r/">R</a></li>
      
        <li><a href="/user/languages/ruby/">Ruby</a></li>
      
        <li><a href="/user/languages/rust/">Rust</a></li>
      
        <li><a href="/user/languages/scala/">Scala</a></li>
      
        <li><a href="/user/languages/smalltalk/">Smalltalk</a></li>
      
        <li><a href="/user/languages/objective-c/">Swift</a></li>
      
        <li><a href="/user/languages/csharp/">Visual Basic</a></li>
      
        <li><a href="/user/languages/community-supported-languages/">Adding a language</a></li>
      </ul>

      <h3>Deployments and Uploads</h3>
      <ul>
        
          <li class="is-overview"><a href="/user/deployment/">Overview</a></li>
          
        
        
          <li><a href="/user/deployment/anynines/">anynines</a></li>
        
          <li><a href="/user/deployment/atlas/">Atlas</a></li>
        
          <li><a href="/user/deployment/codedeploy/">AWS CodeDeploy</a></li>
        
          <li><a href="/user/deployment/elasticbeanstalk/">AWS Elastic Beanstalk</a></li>
        
          <li><a href="/user/deployment/lambda/">AWS Lambda</a></li>
        
          <li><a href="/user/deployment/opsworks/">AWS OpsWorks</a></li>
        
          <li><a href="/user/deployment/s3/">AWS S3</a></li>
        
          <li><a href="/user/deployment/azure-web-apps/">Azure Web Apps</a></li>
        
          <li><a href="/user/deployment/bintray/">bintray</a></li>
        
          <li><a href="/user/deployment/bitballoon/">BitBalloon</a></li>
        
          <li><a href="/user/deployment/bluemixcloudfoundry/">Bluemix CloudFoundry</a></li>
        
          <li><a href="/user/deployment/boxfuse/">Boxfuse</a></li>
        
          <li><a href="/user/deployment/catalyze/">Catalyze</a></li>
        
          <li><a href="/user/deployment/chefsupermarket/">Chef Supermarket</a></li>
        
          <li><a href="/user/deployment/cloud66/">Cloud 66</a></li>
        
          <li><a href="/user/deployment/cloudfoundry/">CloudFoundry</a></li>
        
          <li><a href="/user/deployment/cargo/">Cargo</a></li>
        
          <li><a href="/user/deployment/engineyard/">Engine Yard</a></li>
        
          <li><a href="/user/deployment/pages/">GitHub Pages</a></li>
        
          <li><a href="/user/deployment/releases/">GitHub Releases</a></li>
        
          <li><a href="/user/deployment/google-app-engine/">Google App Engine</a></li>
        
          <li><a href="/user/deployment/gcs/">Google Cloud Storage</a></li>
        
          <li><a href="/user/deployment/firebase/">Google Firebase</a></li>
        
          <li><a href="/user/deployment/hackage/">Hackage</a></li>
        
          <li><a href="/user/deployment/hephy/">Hephy</a></li>
        
          <li><a href="/user/deployment/heroku/">Heroku</a></li>
        
          <li><a href="/user/deployment/launchpad/">Launchpad</a></li>
        
          <li><a href="/user/deployment/npm/">npm</a></li>
        
          <li><a href="/user/deployment/openshift/">OpenShift</a></li>
        
          <li><a href="/user/deployment/packagecloud/">packagecloud.io</a></li>
        
          <li><a href="/user/deployment/puppetforge/">Puppet Forge</a></li>
        
          <li><a href="/user/deployment/pypi/">PyPI</a></li>
        
          <li><a href="/user/deployment/cloudfiles/">Rackspace Cloud Files</a></li>
        
          <li><a href="/user/deployment/rubygems/">RubyGems</a></li>
        
          <li><a href="/user/deployment/scalingo/">Scalingo</a></li>
        
          <li><a href="/user/deployment/script/">Script</a></li>
        
          <li><a href="/user/deployment/snaps/">Snap Store</a></li>
        
          <li><a href="/user/deployment/surge/">Surge.sh</a></li>
        
          <li><a href="/user/deployment/testfairy/">TestFairy</a></li>
        
          <li><a href="/user/deployment/transifex/">Transifex</a></li>
        
        <li><a href="/user/uploading-artifacts/">Uploading Build Artifacts</a></li>
      </ul>

      <h3>CI Environment Reference</h3>
      <ul>
        <li class="is-overview"><a href="/user/reference/overview/">Overview</a></li>
        <li><a href="/user/reference/linux/">Ubuntu Linux CI Environment References</a></li>
        <li><a href="/user/reference/osx/">macOS CI Environment Reference</a></li>
        <li><a href="/user/reference/windows/">Windows CI Environment Reference</a></li>
        <li><a href="/user/reference/freebsd/">FreeBSD CI Environment Reference</a></li>
        <li><a href="/user/multi-os/">Building on Multiple Operating Systems</a></li>
        <li><a href="/user/multi-cpu-architectures/">Building on Multiple CPU Architectures</a></li>
        <li><a href="/user/environment-variables/">Environment Variables</a></li>
        <li><a href="/user/build-environment-updates/">Build Environment Updates</a></li>
        <li><a href="/user/ip-addresses/">Build Machines IP Addresses</a></li>
      </ul>

      <h3>Encrypting Files and Data</h3>
      <ul>
        <li><a href="/user/encryption-keys/">Encrypting Sensitive Data</a></li>
        <li><a href="/user/encrypting-files/">Encrypting Files</a></li>
        <li><a href="/user/github-oauth-scopes/">GitHub Permissions used by Travis CI</a></li>
        <li><a href="/user/bb-oauth-scopes/">Bitbucket Permissions used by Travis CI</a></li>
        <li><a href="/user/assembla-oauth-scopes/">Assembla Permissions used by Travis CI</a></li>
        <li><a href="/user/gl-oauth-scopes/">GitLab Permissions used by Travis CI</a></li>
        <li><a href="/user/best-practices-security/">Best Practices in Securing Your Data</a></li>
      </ul>

      <h3>Integrations and Notifications</h3>
      <ul>
        <li><a href="/user/notifications/">Configuring Notifications</a></li>
        <li><a href="/user/status-images/">Showing Build Status Images</a></li>
        <li><a href="/user/code-climate/">Code Climate</a></li>
        <li><a href="/user/deepsource/">DeepSource</a></li>
        <li><a href="/user/coveralls/">Coveralls</a></li>
        <li><a href="/user/coverity-scan/">Coverity Scan</a></li>
        <li><a href="/user/browserstack/">BrowserStack</a></li>
        <li><a href="/user/sauce-connect/">Sauce Labs</a></li>
        <li><a href="/user/sonarcloud/">SonarCloud</a></li>
        <li><a href="/user/sourceclear/">SourceClear</a></li>
        <li><a href="/user/build-feeds/">Atom Feeds</a></li>
        <li><a href="/user/cc-menu/">CCMenu / CCTray Feeds</a></li>
        <li><a href="/user/integration/platformio/">Embedded Builds with PlatformIO</a></li>
        <li><a href="/user/apps/">3rd Party Apps, Clients and Tools</a></li>
      </ul>

      <h3>Developer Program</h3>
      <ul>
        <li class="is-overview"><a href="/user/developer/">Overview</a></li>
        <li><a href="https://developer.travis-ci.com/">API V3</a></li>
        <li><a href="/user/triggering-builds/">Triggering Builds with API V3</a></li>
        <li><a href="https://github.com/travis-ci/travis#ruby-library">The Ruby Library</a></li>
      </ul>
      
      <h3>Hosted Billing</h3>
      <ul>
        <li class="is-overview"><a href="/user/billing-overview/">Overview</a></li>
        <li><a href="/user/billing-faq/">FAQ</a></li>
      </ul>

      <h3>Travis CI Enterprise</h3>
      <ul>
        <li><a href="/user/enterprise/">Enterprise Docs</a></li>
      </ul>

    </nav>
  </section>

  <section class="sidebar-notice">
    <p>This documentation site is open source.
      The <a href="https://github.com/travis-ci/docs-travis-ci-com">README in our Git repository</a> explains how to contribute.</p>
  </section>

</div><!-- /#sidebar -->


        <main id="main" class="main" data-swiftype-index='true'>
          

          


          

          <section class="index-intro">
  <figure>
    <img src="images/ui/docs-desktop.svg" alt="Image of desktop">
  </figure>
  <h1>New around here? Let's get you going.</h1>
  <p><a class="get-started-button" href="/user/tutorial/">Travis CI Tutorial</a></p>
</section>

<div class="row index-main">
  <div class="columns large-6">
    <section>
      <h2>Already know the basics?</h2>
      <ul>
        <li><a href="/user/customizing-the-build">Customizing your build</a></li>
      </ul>
    </section>

    <section>
      <h2>Migrate to GitHub Apps</h2>
      <ul>
      <li><a href="/user/migrate/open-source-on-travis-ci-com/">Open source accounts</a></li>
      <li><a href="/user/migrate/legacy-services-to-github-apps-migration-guide/">Private accounts</a></li>
      </ul>
    </section>

    <section>
      <h2>Deployment Guides</h2>
      <p>Learn how to deploy to your hosting provider using Travis CI.</p>
      <ul>
        <li><a href="/user/deployment/heroku/">Heroku</a></li>
        <li><a href="/user/deployment/codedeploy/">AWS CodeDeploy</a></li>
        <li><a href="/user/deployment/openshift/">OpenShift</a></li>
        <li><a href="/user/deployment">more deployment guides</a></li>
      </ul>
    </section>
    <section>
      <h2>Common Questions</h2>
      <ul>
        <li><a href="/user/customizing-the-build/#what-repository-providers-or-version-control-systems-can-i-use">What repository providers can I use?</a></li>
        <li><a href="/user/reference/bionic/">Can I use Ubuntu 18.04 Bionic for my builds?</a></li>
        <li><a href="/user/reference/xenial/">Can I use Ubuntu Xenial for my builds?</a><li>
        <li><a href="/user/notifications/#changing-the-email-address-for-build-notifications">How do I change the notification email address?</a><li>
        <li><a href="/user/ip-addresses/">What IP addresses do the builds use?</a></li>
      </ul>
    </section>
  </div>
  <div class="columns large-6">
    <section>
      <h2>Language-specific Guides</h2>
      <p>Learn about using Travis CI with your favourite programming language.</p>
      <div class="language-docs" >

      <ul class="list-language">

  <li><a href="/user/languages/android/">Android</a></li>

  <li><a href="/user/languages/c/">C</a></li>

  <li><a href="/user/languages/csharp/">C#</a></li>

  <li><a href="/user/languages/cpp/">C++</a></li>

  <li><a href="/user/languages/clojure/">Clojure</a></li>

  <li><a href="/user/languages/crystal/">Crystal</a></li>

  <li><a href="/user/languages/d/">D</a></li>

  <li><a href="/user/languages/dart/">Dart</a></li>

  <li><a href="/user/languages/elixir/">Elixir</a></li>

  <li><a href="/user/languages/elm/">Elm</a></li>

  <li><a href="/user/languages/erlang/">Erlang</a></li>

  <li><a href="/user/languages/csharp/">F#</a></li>

  <li><a href="/user/languages/minimal-and-generic/">Generic</a></li>

  <li><a href="/user/languages/go/">Go</a></li>

  <li><a href="/user/languages/groovy/">Groovy</a></li>

  <li><a href="/user/languages/haskell/">Haskell</a></li>

  <li><a href="/user/languages/haxe/">Haxe</a></li>

  <li><a href="/user/languages/java/">Java</a></li>

  <li><a href="/user/languages/javascript-with-nodejs/">JavaScript <small>(with Node.js)</small></a></li>

  <li><a href="/user/languages/julia/">Julia</a></li>

  <li><a href="/user/languages/matlab/">MATLAB</a></li>

  <li><a href="/user/languages/minimal-and-generic/">Minimal</a></li>

  <li><a href="/user/languages/nix/">Nix</a></li>

  <li><a href="/user/languages/objective-c/">Objective-C</a></li>

  <li><a href="/user/languages/perl/">Perl</a></li>

  <li><a href="/user/languages/perl6/">Perl6</a></li>

  <li><a href="/user/languages/php/">PHP</a></li>

  <li><a href="/user/languages/python/">Python</a></li>

  <li><a href="/user/languages/r/">R</a></li>

  <li><a href="/user/languages/ruby/">Ruby</a></li>

  <li><a href="/user/languages/rust/">Rust</a></li>

  <li><a href="/user/languages/scala/">Scala</a></li>

  <li><a href="/user/languages/smalltalk/">Smalltalk</a></li>

  <li><a href="/user/languages/objective-c/">Swift</a></li>

  <li><a href="/user/languages/csharp/">Visual Basic</a></li>

</ul>

      </div>
    </section>
  </div>
</div>


        </main>
      </div><!-- /#content -->

      <footer class="footer">
  <div class="layout-inner">
    <div class="footer-elem">
      <svg width="142px" height="45.381px" viewBox="0 0 142 45.381" enable-background="new 0 0 142 45.381" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink= "http://www.w3.org/1999/xlink">
        <title>Travis CI Mascot</title>
        <image xlink:href="https://styleguide.travis-ci.com/images/logos/travis-footer-logo-new.svg" x="0" y="0" width="142px" height="45.381px" />
      </svg>
    </div>

    <div class="footer-elem"></div>

    <div class="footer-elem">
      <h3 class="footer-title">©Travis CI, GmbH</h3>
      <address>Rigaer Straße 8<br>10247 Berlin, Germany</address>
      <ul>
        <li><a href="https://travisci.workable.com/" title="Jobs at Travis CI">Work with Travis CI</a></li>
      </ul>
    </div>

    <div class="footer-elem">
      <h3 class="footer-title">Help</h3>
      <ul>
        <li><a href="https://docs.travis-ci.com" title="Travis CI Docs">Documentation</a></li>
        <li><a href="https://changelog.travis-ci.com/">Changelog</a></li>
        <li><a href="https://blog.travis-ci.com/" title="Travis CI Blog">Blog</a></li>
        <li><a href="mailto:support@travis-ci.com" title="Email Travis CI support">Email</a></li>
        <li><a href="https://twitter.com/travisci" title="Travis CI on Twitter">Twitter</a></li>
      </ul>
    </div>

    <div class="footer-elem">
      <h3 class="footer-title">Company</h3>
      <ul>
        <li><a href="/imprint.html" title="Imprint">Imprint</a></li>
        <li><a href="https://ideracorp.com/legal/TravisCI">Legal</a></li>
      </ul>
    </div>

    <div class="footer-elem">
      <h3 class="footer-title">Travis CI Status</h3>
      <ul>
        <li><div class="status-circle status">Status:</div>
          <a href="http://www.traviscistatus.com/">Travis CI Status</a>
        </li>
      </ul>
    </div>
  </div>
</footer>

<script>
 fetch('https://pnpcptp8xh9k.statuspage.io/api/v2/status.json').then(function(response) {
   return response.json();
 }).then(function(data) {
   if (data.status && data.status.indicator) {
     document.querySelector('.status-circle').classList.add(data.status.indicator);
   }
 });
</script>

    </div><!-- /.wrapper -->
  </body>
</html>
